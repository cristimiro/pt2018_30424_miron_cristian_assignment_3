package customerOrderProduct;
/**
 * This is the class implementing a Product
 * @author Cristian Miorn
 *
 */
public class Product {
	
	private int productId;
	private String productName;
	private int productPrice;
	private int productStock;
	
	/**
	 * The product details:
	 * @param productId id of the product
	 * @param productName name of the product
	 * @param productPrice price of the product
	 * @param productStock stock of the product
	 */
	
	public Product(int productId, String productName, int productPrice, int productStock) {
		this.productId=productId;
		this.productName=productName;
		this.productPrice=productPrice;
		this.productStock=productStock;
	}

	public Product() {
		// TODO Auto-generated constructor stub
	}
/**
 * returns product id
 * @return
 */
	public int getProductId() {
		return productId;
	}
/**
 * sets product id
 * @param productId
 */
	public void setProductId(int productId) {
		this.productId = productId;
	}
/**
 * returns product Name
 * @return
 */
	public String getProductName() {
		return productName;
	}
/**
 * sets product Name
 * @param productName
 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
/**
 * returns product Price
 * @return
 */
	public int getProductPrice() {
		return productPrice;
	}
/**
 * sets product price
 * @param productPrice
 */
	public void setProductPrice(int productPrice) {
		this.productPrice = productPrice;
	}
/**
 * returns product Stock
 * @return
 */
	public int getProductStock() {
		return productStock;
	}
/**
 * sets Product Stock
 * @param productStock
 */
	public void setProductStock(int productStock) {
		this.productStock = productStock;
	}
	/**
	 * Overrides the "toString" Method
	 * @Override
	 */
	public String toString() {
		String s = "";
		s = " ID - "+getProductId()+", Name - "+getProductName()+", Price - "+getProductPrice()+".";
		return s;
	}

	
	
}
