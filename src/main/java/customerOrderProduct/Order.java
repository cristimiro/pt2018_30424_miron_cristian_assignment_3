package customerOrderProduct;
/**
 * This is the class implementing an Order
 * @author Cristian Miorn
 *
 */
public class Order {

	private int orderId;
	private int orderClientId;
	private int orderProductId;
	private int orderQuantity;
	
	/**
	 * The order Details:
	 * @param orderId
	 * @param orderClientId
	 * @param orderProductId
	 * @param orderQuantity
	 */
	
	public Order(int orderId, int orderClientId, int orderProductId, int orderQuantity) {
		this.orderId=orderId;
		this.orderClientId=orderClientId;
		this.orderProductId=orderProductId;
		this.orderQuantity=orderQuantity;
	}

	public Order() {
		// TODO Auto-generated constructor stub
	}
/**
 * returns the order id
 * @return
 */
	public int getOrderId() {
		return orderId;
	}
/**
 * Sets a new order Id
 * @param orderId
 */
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	/**
	 * returns the client Id from the order
	 * @return
	 */
	public int getOrderClientId() {
		return orderClientId;
	}
	/**
	 * Sets a new client Id for the order
	 * @param orderId
	 */
	public void setOrderClientId(int orderClientId) {
		this.orderClientId = orderClientId;
	}
	/**
	 * returns the product ID from the order
	 * @return
	 */
	public int getOrderProductId() {
		return orderProductId;
	}
	/**
	 * Sets a new product ID for the order
	 * @param orderId
	 */
	public void setOrderProductId(int orderProductId) {
		this.orderProductId = orderProductId;
	}
	/**
	 * returns the order quantity
	 * @return
	 */
	public int getOrderQuantity() {
		return orderQuantity;
	}
	/**
	 * Sets a new order quantity
	 * @param orderId
	 */
	public void setOrderQuantity(int orderQuantity) {
		this.orderQuantity = orderQuantity;
	}
	/**
	 * Overrides the "toString" Method
	 * @Override
	 */
	public String toString() {
		String s = "";
		s = "Id - "+getOrderId()+", Client ID - "+getOrderClientId()+", Product ID - "+getOrderProductId()+", Quantity - "+getOrderQuantity()+".";
		return s;
	}
	
}
