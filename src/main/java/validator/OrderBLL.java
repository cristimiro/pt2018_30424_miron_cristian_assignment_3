package validator;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import validator.QuantityValidator;
import validator.Validator;
import dao.ClientDAO;
import dao.OrderDao;
import customerOrderProduct.Client;
import customerOrderProduct.Order;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class OrderBLL {

	private ArrayList<Validator<Order>> validators;

	public OrderBLL() {
		validators = new ArrayList<Validator<Order>>();
		validators.add(new QuantityValidator());
	}

	public Order findOrderById(int id) {
		Order st = OrderDao.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The order with id =" + id + " was not found!");
		}
		return st;
	}

	public int insertOrder(Order order) {
		for (Validator<Order> v : validators) {
			v.validate(order);
		}
		return OrderDao.insert(order);
	}
}
