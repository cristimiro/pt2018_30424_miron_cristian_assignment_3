package validator;

import customerOrderProduct.Order;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class QuantityValidator implements Validator<Order>{
	private static final int MIN_QUANTITY = 0;

	public void validate(Order t) {
		if (t.getOrderQuantity() < MIN_QUANTITY) {
			throw new IllegalArgumentException("Empty Stock!");
		}
	}
	public void check() {
		
	}

}
