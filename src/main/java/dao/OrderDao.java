package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import customerOrderProduct.Order;

/**
 *  This is the class where order operation take place
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class OrderDao {

	protected static final Logger LOGGER = Logger.getLogger(OrderDao.class.getName());
	private static final String insertStatementString = "INSERT INTO homework3.order (orderClientId,orderProductId,orderQuantity) VALUES (?,?,?)";
//	private static final String updateStatementString = "UPDATE order SET productName = ? , productPrice = ? , productStock = ? "
//			+" WHERE productId = ?";
	private final static String findStatementString = "SELECT * FROM homework3.order where orderId = ?";
	private final static String findAllStatementString = "SELECT * FROM homework3.order";
	private final static String removeStatementString = "delete from homework3.order where orderId = ?";

	/**
	 * Returns the order with the id given as parameter
	 * @param orderId
	 * @return
	 */
	public static Order findById(int orderId) {
		Order toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, orderId);
			rs = findStatement.executeQuery();
			rs.next();

			int orderClientId= rs.getInt("orderClientId");
			int orderProductId = rs.getInt("orderProductId");
			int orderQuantity = rs.getInt("orderQuantity");
			toReturn = new Order(orderId, orderClientId, orderProductId, orderQuantity);
			//System.out.println(toReturn);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDao:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
/**
 * returns an arrayList containing all the orders
 * @return arrayList
 */
	public static ArrayList<Order> findAll() {
		Order toReturn = null;
		ArrayList<Order> orderList = new ArrayList<Order>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findAllStatementString);
			rs = findStatement.executeQuery();
			while(rs.next()) {
				int orderId = rs.getInt("orderId");
				int orderClientId= rs.getInt("orderClientId");
				int orderProductId = rs.getInt("orderProductId");
				int orderQuantity = rs.getInt("orderQuantity");
				toReturn = new Order(orderId, orderClientId, orderProductId, orderQuantity);
				orderList.add(toReturn);
				//System.out.println(toReturn);
			};
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDao:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return orderList;
	}
	/**
	 * inserts a new order
	 * @param order order to be inserted
	 * @return the inserted order
	 */
	public static int insert(Order order) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, order.getOrderClientId());
			insertStatement.setInt(2, order.getOrderProductId());
			insertStatement.setInt(3, order.getOrderQuantity());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDao:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	/**
	 * removes an order
	 * @param orderId id of the order to be removed
	 */
	public static void remove(int orderId) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement removeStatement = null;
		try {
			removeStatement = dbConnection.prepareStatement(removeStatementString, Statement.RETURN_GENERATED_KEYS);
			removeStatement.setInt(1, orderId);
			removeStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDao:remove " + e.getMessage());
		} finally {
			ConnectionFactory.close(removeStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * returns an arrayList containing all the orders
	 * @return arrayList
	 */
	
}


