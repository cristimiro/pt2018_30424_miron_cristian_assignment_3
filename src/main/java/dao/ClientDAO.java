package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import connection.ConnectionFactory;
import customerOrderProduct.Client;
import userInterface.functionFrame;

/**
 *  This is the class where client operation take place
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class ClientDAO {

	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO client (clientName,clientMail,clientPhoneNumber,clientAge)"
			+ " VALUES (?,?,?,?)";
	private static final String updateStatementString = "UPDATE client SET clientName = ? , clientMail = ? , "
			+ "clientPhoneNumber = ? , clientAge = ?  WHERE clientId = ?";
	private final static String findStatementString = "SELECT * FROM client where clientId = ?";
	private final static String removeStatementString = "delete from client where clientId = ?";
	private final static String findAllStatementString = "SELECT * FROM homework3.client";

	
	/**
	 * Returns the client with the id given as parameter
	 * @param clientId
	 * @return
	 */
	
	public static Client findById(int clientId) {
		Client toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, clientId);
			rs = findStatement.executeQuery();
			rs.next();

			String clientName = rs.getString("clientName");
			String clientMail = rs.getString("clientMail");
			String clientPhoneNumber = rs.getString("clientPhoneNumber");
			int clientAge = rs.getInt("clientAge");
			toReturn = new Client(clientId, clientName, clientMail, clientPhoneNumber, clientAge);
			//System.out.println(toReturn);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	/**
	 * returns an arrayList of all the clients
	 * @return
	 */
	public static ArrayList<Client> findAll() {
		Client toReturn = null;
		ArrayList<Client> clientList = new ArrayList<Client>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findAllStatementString);
			rs = findStatement.executeQuery();
			while(rs.next()) {
				int clientId = rs.getInt("clientId");
				String clientName = rs.getString("clientName");
				String clientMail = rs.getString("clientMail");
				String clientPhoneNumber = rs.getString("clientPhoneNumber");
				int clientAge = rs.getInt("clientAge");
				toReturn = new Client(clientId, clientName, clientMail, clientPhoneNumber, clientAge);
				clientList.add(toReturn);
				//System.out.println(toReturn);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDao:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return clientList;
	}
/**
 * inserts a new client to the database
 * @param client client with all its details
 * @return
 */
	public static int insert(Client client) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, client.getClientName());
			insertStatement.setString(2, client.getClientMail());
			insertStatement.setString(3, client.getClientPhoneNumber());
			insertStatement.setInt(4, client.getClientAge());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "StudentDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	/**
	 * removes a client from the database
	 * @param clientId the Id of the client to be removed
	 */
	public static void remove(int clientId) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement removeStatement = null;
		try {
			removeStatement = dbConnection.prepareStatement(removeStatementString, Statement.RETURN_GENERATED_KEYS);
			removeStatement.setInt(1, clientId);
			removeStatement.executeUpdate();

			//ResultSet rs = removeStatement.getGeneratedKeys();

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "StudentDAO:remove " + e.getMessage());
		} finally {
			ConnectionFactory.close(removeStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * updates a client
	 * @param givenIdToUpdate the id of the client that will be updated
	 * @param client - the client details
	 * @return returns an updated client
	 */
	public static int update(int givenIdToUpdate,Client client) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement updateStatement = null;
		int insertedId = -1;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setString(1, client.getClientName());
			updateStatement.setString(2, client.getClientMail());
			updateStatement.setString(3, client.getClientPhoneNumber());
			updateStatement.setInt(4, client.getClientAge());
			updateStatement.setInt(5, givenIdToUpdate);
			updateStatement.executeUpdate();

			ResultSet rs = updateStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "StudentDAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	
}

