package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import connection.ConnectionFactory;
import customerOrderProduct.Order;
import customerOrderProduct.Product;
import userInterface.functionFrame;

/**
 *  This is the class where product operation take place
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class ProductDao {

	protected static final Logger LOGGER = Logger.getLogger(ProductDao.class.getName());
	private static final String insertStatementString = "INSERT INTO product (productName,productPrice,productStock) VALUES (?,?,?)";
	private static final String updateStatementString = "UPDATE product SET productName = ? , productPrice = ? , productStock = ? "
			+" WHERE productId = ?";
	private final static String findStatementString = "SELECT * FROM product where productId = ?";
	private final static String removeStatementString = "delete from product where productId = ?";
	private final static String findAllStatementString = "SELECT * FROM homework3.product";
	
	/**
	 * Returns the product with the id given as parameter
	 * @param productId
	 * @return
	 */
	public static Product findById(int productId) {
		Product toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, productId);
			rs = findStatement.executeQuery();
			rs.next();

			String productName = rs.getString("productName");
			int productPrice = rs.getInt("productPrice");
			int productStock = rs.getInt("productStock");
			toReturn = new Product(productId, productName, productPrice, productStock);
			//System.out.println(toReturn);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDao:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	/**
	 * selects all the products into one list
	 * @return returns an arrayList containing all the products
	 */
	public static ArrayList<Product> findAll() {
		Product toReturn = null;
		ArrayList<Product> productList = new ArrayList<Product>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findAllStatementString);
			rs = findStatement.executeQuery();
			while(rs.next()) {
				int productId = rs.getInt("productId");
				String productName = rs.getString("productName");
				int productPrice = rs.getInt("productPrice");
				int productStock = rs.getInt("productStock");
				toReturn = new Product(productId, productName, productPrice, productStock);
				productList.add(toReturn);
				//System.out.println(toReturn);
			}		
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDao:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return productList;
	}
	/**
	 * Inserts a new product
	 * @param product The product to be inserted
	 * @return the inserted product
	 */
	public static int insert(Product product) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, product.getProductName());
			insertStatement.setInt(2, product.getProductPrice());
			insertStatement.setInt(3, product.getProductStock());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDao:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	/**
	 * removes a product from the list
	 * @param productId the id of the product to be removed
	 */
	public static void remove(int productId) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement removeStatement = null;
		try {
			removeStatement = dbConnection.prepareStatement(removeStatementString, Statement.RETURN_GENERATED_KEYS);
			removeStatement.setInt(1, productId);
			removeStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDao:remove " + e.getMessage());
		} finally {
			ConnectionFactory.close(removeStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * Updates a product
	 * @param givenIdToUpdate id of the product to pe updated
	 * @param product details of the product to be updated
	 * @return the updated product
	 */
	public static int update(int givenIdToUpdate,Product product) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement updateStatement = null;
		int insertedId = -1;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setString(1, product.getProductName());
			updateStatement.setInt(2, product.getProductPrice());
			updateStatement.setInt(3, product.getProductStock());
			updateStatement.setInt(4, givenIdToUpdate);
			updateStatement.executeUpdate();

			ResultSet rs = updateStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDao:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	
}


