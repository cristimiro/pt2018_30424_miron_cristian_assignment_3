package userInterface;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import customerOrderProduct.Client;
import customerOrderProduct.Order;
import customerOrderProduct.Product;
import dao.ClientDAO;
import dao.OrderDao;
import dao.ProductDao;
import start.JTabelValuePopulator;
/**
 * This is the user interface class
 * @author Cristian Miorn
 *
 */
public class functionFrame extends JFrame {
	private static final long serialVersionUID = 1L;

	// LABELS
	JLabel clientIdLabel = new JLabel("clientId");
	JLabel clientNameLabel = new JLabel("Name");
	JLabel clientMailLabel = new JLabel("Mail");
	JLabel clientPhoneLabel = new JLabel("Phone no.");
	JLabel clientAgeLabel = new JLabel("Age");

	JLabel productIdLabel = new JLabel("productId");
	JLabel productNameLabel = new JLabel("Name");
	JLabel productPriceLabel = new JLabel("Price");
	JLabel productStockLabel = new JLabel("Stock");

	JLabel orderIdLabel = new JLabel("orderId");
	JLabel orderClientIdLabel = new JLabel("Client Id");
	JLabel orderProductIdLabel = new JLabel("Product Id");
	JLabel orderQuantityLabel = new JLabel("Stock");

	// BUTTONS

	JButton clientButton = new JButton("Client"); // Creat all the buttons, textfields and labels you need
	JButton productButton = new JButton("Product");
	JButton orderButton = new JButton("Order");

	JButton clientAddButton = new JButton("Add Client");
	JButton clientRemoveButton = new JButton("Remove Client");
	JButton clientUpdateButton = new JButton("Update Client");
	JButton clientBackButton = new JButton("Back to menu");

	JButton productAddButton = new JButton("Add Product");
	JButton productRemoveButton = new JButton("Remove Product");
	JButton productUpdateButton = new JButton("Update Product");
	JButton productBackButton = new JButton("Back to menu");

	JButton orderAddButton = new JButton("Add Order");
	JButton orderRemoveButton = new JButton("Remove Order");
	JButton orderBackButton = new JButton("Back to menu");
	JButton orderPrint = new JButton("Print Order");

	// TEXT FIELDS
	JTextField clientIdTextField = new JTextField();
	JTextField clientNameTextField = new JTextField();
	JTextField clientMailTextField = new JTextField();
	JTextField clientPhoneTextField = new JTextField();
	JTextField clientAgeTextField = new JTextField();

	JTextField productIdTextField = new JTextField();
	JTextField productNameTextField = new JTextField();
	JTextField productPriceTextField = new JTextField();
	JTextField productStockTextField = new JTextField();

	JTextField orderIdTextField = new JTextField();
	JTextField orderClientIdTextField = new JTextField();
	JTextField orderProductIdTextField = new JTextField();
	JTextField orderQuantityTextField = new JTextField();

	// Tables

	public static final JPanel mainPanel = new JPanel();
/**
 * The constructor of the graphical interface.
 * @throws ClassNotFoundException
 * @throws InstantiationException
 * @throws IllegalAccessException
 * @throws UnsupportedLookAndFeelException
 */
	public functionFrame() throws ClassNotFoundException, InstantiationException, IllegalAccessException,
			UnsupportedLookAndFeelException {

		UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");

		setSize(600, 600);// size of the panel
		setTitle("Order Manager"); // title
		setResizable(false); // not allows the user to extend the window

		// create panel

		mainPanel.setLayout(null);
		// Main menu
		mainPanel.add(clientButton);
		mainPanel.add(productButton);
		mainPanel.add(orderButton);
		clientButton.setBounds(150, 100, 300, 80);
		clientButton.setForeground(Color.BLACK);
		clientButton.setFont(clientButton.getFont().deriveFont(20.0f));
		productButton.setBounds(150, 250, 300, 80);
		productButton.setForeground(Color.BLACK);
		productButton.setFont(productButton.getFont().deriveFont(20.0f));
		orderButton.setBounds(150, 400, 300, 80);
		orderButton.setForeground(Color.BLACK);
		orderButton.setFont(orderButton.getFont().deriveFont(20.0f));

		clientButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final JPanel clientPanel = new JPanel();
				clientPanel.setLayout(null);
				clientPanel.setVisible(true);
				mainPanel.setVisible(false);
				setContentPane(clientPanel);
				setSize(720, 700);
				setTitle("Clients");

				// Client
				clientPanel.add(clientIdLabel);
					clientIdLabel.setBounds(40, 400, 100, 40);
					clientIdLabel.setForeground(Color.BLACK);
					clientIdLabel.setFont(clientIdLabel.getFont().deriveFont(20.0f));
				clientPanel.add(clientNameLabel);
					clientNameLabel.setBounds(180, 400, 100, 40);
					clientNameLabel.setForeground(Color.BLACK);
					clientNameLabel.setFont(clientNameLabel.getFont().deriveFont(20.0f));
				clientPanel.add(clientMailLabel);
					clientMailLabel.setBounds(320, 400, 100, 40);
					clientMailLabel.setForeground(Color.BLACK);
					clientMailLabel.setFont(clientMailLabel.getFont().deriveFont(20.0f));
				clientPanel.add(clientPhoneLabel);
					clientPhoneLabel.setBounds(460, 400, 100, 40);
					clientPhoneLabel.setForeground(Color.BLACK);
					clientPhoneLabel.setFont(clientPhoneLabel.getFont().deriveFont(20.0f));
				clientPanel.add(clientAgeLabel);
					clientAgeLabel.setBounds(600, 400, 100, 40);
					clientAgeLabel.setForeground(Color.BLACK);
					clientAgeLabel.setFont(clientAgeLabel.getFont().deriveFont(20.0f));
				clientPanel.add(clientIdTextField);
					clientIdTextField.setBounds(40, 460, 100, 40);
					clientIdTextField.setForeground(Color.BLACK);
					clientIdTextField.setFont(clientIdTextField.getFont().deriveFont(20.0f));
				clientPanel.add(clientNameTextField);
					clientNameTextField.setBounds(180, 460, 100, 40);
					clientNameTextField.setForeground(Color.BLACK);
					clientNameTextField.setFont(clientNameTextField.getFont().deriveFont(20.0f));
				clientPanel.add(clientMailTextField);
					clientMailTextField.setBounds(320, 460, 100, 40);
					clientMailTextField.setForeground(Color.BLACK);
					clientMailTextField.setFont(clientMailTextField.getFont().deriveFont(20.0f));
				clientPanel.add(clientPhoneTextField);
					clientPhoneTextField.setBounds(460, 460, 100, 40);
					clientPhoneTextField.setForeground(Color.BLACK);
					clientPhoneTextField.setFont(clientPhoneTextField.getFont().deriveFont(20.0f));
				clientPanel.add(clientAgeTextField);
					clientAgeTextField.setBounds(600, 460, 100, 40);
					clientAgeTextField.setForeground(Color.BLACK);
					clientAgeTextField.setFont(clientAgeTextField.getFont().deriveFont(20.0f));
				clientPanel.add(clientAddButton);
					clientAddButton.setBounds(40, 560, 150, 40);
					clientAddButton.setForeground(Color.BLACK);
					clientAddButton.setFont(clientAddButton.getFont().deriveFont(17.0f));
				clientPanel.add(clientRemoveButton);
					clientRemoveButton.setBounds(200, 560, 150, 40);
					clientRemoveButton.setForeground(Color.BLACK);
					clientRemoveButton.setFont(clientRemoveButton.getFont().deriveFont(17.0f));
				clientPanel.add(clientUpdateButton);
					clientUpdateButton.setBounds(360, 560, 150, 40);
					clientUpdateButton.setForeground(Color.BLACK);
					clientUpdateButton.setFont(clientUpdateButton.getFont().deriveFont(17.0f));
				clientPanel.add(clientBackButton);
					clientBackButton.setBounds(520, 560, 150, 40);
					clientBackButton.setForeground(Color.BLACK);
					clientBackButton.setFont(clientBackButton.getFont().deriveFont(17.0f));
				JTabelValuePopulator pop = new JTabelValuePopulator();
				JTable createTable = pop.createTable(Client.class, ClientDAO.findAll());
				clientPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);

				clientBackButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						clientPanel.setVisible(false);
						mainPanel.setVisible(true);
						setContentPane(mainPanel);
						setSize(600, 600);
						setTitle("Order Manager");
					}

				});
				clientAddButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						ClientDAO client = new ClientDAO();
						client.insert(new Client(Integer.parseInt(clientIdTextField.getText()),
								clientNameTextField.getText(), clientMailTextField.getText(),
								clientPhoneTextField.getText(), Integer.parseInt(clientAgeTextField.getText())));
						clientIdTextField.setText("");
						clientNameTextField.setText("");
						clientMailTextField.setText("");
						clientPhoneTextField.setText("");
						clientAgeTextField.setText("");
						JTabelValuePopulator pop = new JTabelValuePopulator();
						JTable createTable = pop.createTable(Client.class, ClientDAO.findAll());
						clientPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
					}
				});

				clientUpdateButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						ClientDAO client = new ClientDAO();
						client.update(Integer.parseInt(clientIdTextField.getText()),
								new Client(Integer.parseInt(clientIdTextField.getText()), clientNameTextField.getText(),
										clientMailTextField.getText(), clientPhoneTextField.getText(),
										Integer.parseInt(clientAgeTextField.getText())));
						clientIdTextField.setText("");
						clientNameTextField.setText("");
						clientMailTextField.setText("");
						clientPhoneTextField.setText("");
						clientAgeTextField.setText("");
						JTabelValuePopulator pop = new JTabelValuePopulator();
						JTable createTable = pop.createTable(Client.class, ClientDAO.findAll());
						clientPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
					}
				});

				clientRemoveButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						ClientDAO client = new ClientDAO();
						client.remove(Integer.parseInt(clientIdTextField.getText()));
						clientIdTextField.setText("");
						clientNameTextField.setText("");
						clientMailTextField.setText("");
						clientPhoneTextField.setText("");
						clientAgeTextField.setText("");
						JTabelValuePopulator pop = new JTabelValuePopulator();
						JTable createTable = pop.createTable(Client.class, ClientDAO.findAll());
						clientPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
					}
				});
			}
		});

		productButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final JPanel productPanel = new JPanel();
				productPanel.setLayout(null);
				productPanel.setVisible(true);
				mainPanel.setVisible(false);
				setContentPane(productPanel);
				setSize(720, 700);
				setTitle("Products");

				productPanel.add(productIdLabel);
					productIdLabel.setBounds(40, 400, 100, 40);
					productIdLabel.setForeground(Color.BLACK);
					productIdLabel.setFont(productIdLabel.getFont().deriveFont(20.0f));
				productPanel.add(productNameLabel);
					productNameLabel.setBounds(180, 400, 100, 40);
					productNameLabel.setForeground(Color.BLACK);
					productNameLabel.setFont(productNameLabel.getFont().deriveFont(20.0f));
				productPanel.add(productPriceLabel);
					productPriceLabel.setBounds(320, 400, 100, 40);
					productPriceLabel.setForeground(Color.BLACK);
					productPriceLabel.setFont(productPriceLabel.getFont().deriveFont(20.0f));
				productPanel.add(productStockLabel);
					productStockLabel.setBounds(460, 400, 100, 40);
					productStockLabel.setForeground(Color.BLACK);
					productStockLabel.setFont(productStockLabel.getFont().deriveFont(20.0f));
				productPanel.add(productIdTextField);
					productIdTextField.setBounds(40, 460, 100, 40);
					productIdTextField.setForeground(Color.BLACK);
					productIdTextField.setFont(productIdTextField.getFont().deriveFont(20.0f));
				productPanel.add(productNameTextField);
					productNameTextField.setBounds(180, 460, 100, 40);
					productNameTextField.setForeground(Color.BLACK);
					productNameTextField.setFont(productNameTextField.getFont().deriveFont(20.0f));
				productPanel.add(productPriceTextField);
					productPriceTextField.setBounds(320, 460, 100, 40);
					productPriceTextField.setForeground(Color.BLACK);
					productPriceTextField.setFont(productPriceTextField.getFont().deriveFont(20.0f));
				productPanel.add(productStockTextField);
					productStockTextField.setBounds(460, 460, 100, 40);
					productStockTextField.setForeground(Color.BLACK);
					productStockTextField.setFont(productStockTextField.getFont().deriveFont(20.0f));
				productPanel.add(productAddButton);
					productAddButton.setBounds(40, 560, 130, 40);
					productAddButton.setForeground(Color.BLACK);
					productAddButton.setFont(productAddButton.getFont().deriveFont(16.0f));
					productPanel.add(productRemoveButton);
					productRemoveButton.setBounds(180, 560, 170, 40);
					productRemoveButton.setForeground(Color.BLACK);
					productRemoveButton.setFont(productRemoveButton.getFont().deriveFont(15.0f));
				productPanel.add(productUpdateButton);
					productUpdateButton.setBounds(360, 560, 150, 40);
					productUpdateButton.setForeground(Color.BLACK);
					productUpdateButton.setFont(productUpdateButton.getFont().deriveFont(15.0f));
				productPanel.add(productBackButton);
					productBackButton.setBounds(520, 560, 150, 40);
					productBackButton.setForeground(Color.BLACK);
					productBackButton.setFont(productBackButton.getFont().deriveFont(17.0f));
				JTabelValuePopulator pop = new JTabelValuePopulator();
				JTable createTable = pop.createTable(Product.class, ProductDao.findAll());
				productPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);

				productBackButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						productPanel.setVisible(false);
						mainPanel.setVisible(true);
						setContentPane(mainPanel);
						setSize(600, 600);
						setTitle("Order Manager");
					}
				});
				
				productAddButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						ProductDao product = new ProductDao();
						product.insert(new Product(Integer.parseInt(productIdTextField.getText()),
								productNameTextField.getText(), Integer.parseInt(productPriceTextField.getText()),
								Integer.parseInt(productStockTextField.getText())));
						productIdTextField.setText("");
						productNameTextField.setText("");
						productPriceTextField.setText("");
						productStockTextField.setText("");
						JTabelValuePopulator pop = new JTabelValuePopulator();
						JTable createTable = pop.createTable(Product.class, ProductDao.findAll());
						productPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
					}
				});

				productUpdateButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						ProductDao product = new ProductDao();
						product.update(Integer.parseInt(productIdTextField.getText()),
								new Product(Integer.parseInt(productIdTextField.getText()),
										productNameTextField.getText(),
										Integer.parseInt(productPriceTextField.getText()),
										Integer.parseInt(productStockTextField.getText())));
						productIdTextField.setText("");
						productNameTextField.setText("");
						productPriceTextField.setText("");
						productStockTextField.setText("");
						JTabelValuePopulator pop = new JTabelValuePopulator();
						JTable createTable = pop.createTable(Product.class, ProductDao.findAll());
						productPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
					}
				});

				productRemoveButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						ProductDao product = new ProductDao();
						product.remove(Integer.parseInt(productIdTextField.getText()));
						productIdTextField.setText("");
						productNameTextField.setText("");
						productPriceTextField.setText("");
						productStockTextField.setText("");
						JTabelValuePopulator pop = new JTabelValuePopulator();
						JTable createTable = pop.createTable(Product.class, ProductDao.findAll());
						productPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
					}
				});
			}
		});

		orderButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final JPanel orderPanel = new JPanel();
				orderPanel.setLayout(null);
				orderPanel.setVisible(true);
				mainPanel.setVisible(false);
				setContentPane(orderPanel);
				setSize(720, 700);
				setTitle("Products");

				orderPanel.add(orderIdLabel);
					orderIdLabel.setBounds(40, 400, 100, 40);
					orderIdLabel.setForeground(Color.BLACK);
					orderIdLabel.setFont(orderIdLabel.getFont().deriveFont(20.0f));
				orderPanel.add(orderClientIdLabel);
					orderClientIdLabel.setBounds(180, 400, 100, 40);
					orderClientIdLabel.setForeground(Color.BLACK);
					orderClientIdLabel.setFont(orderClientIdLabel.getFont().deriveFont(20.0f));
				orderPanel.add(orderProductIdLabel);
					orderProductIdLabel.setBounds(320, 400, 100, 40);
					orderProductIdLabel.setForeground(Color.BLACK);
					orderProductIdLabel.setFont(orderProductIdLabel.getFont().deriveFont(20.0f));
				orderPanel.add(orderQuantityLabel);
					orderQuantityLabel.setBounds(460, 400, 100, 40);
					orderQuantityLabel.setForeground(Color.BLACK);
					orderQuantityLabel.setFont(orderQuantityLabel.getFont().deriveFont(20.0f));
				orderPanel.add(orderIdTextField);
					orderIdTextField.setBounds(40, 460, 100, 40);
					orderIdTextField.setForeground(Color.BLACK);
					orderIdTextField.setFont(orderIdTextField.getFont().deriveFont(20.0f));
				orderPanel.add(orderClientIdTextField);
					orderClientIdTextField.setBounds(180, 460, 100, 40);
					orderClientIdTextField.setForeground(Color.BLACK);
					orderClientIdTextField.setFont(orderClientIdTextField.getFont().deriveFont(20.0f));
				orderPanel.add(orderProductIdTextField);
					orderProductIdTextField.setBounds(320, 460, 100, 40);
					orderProductIdTextField.setForeground(Color.BLACK);
					orderProductIdTextField.setFont(orderProductIdTextField.getFont().deriveFont(20.0f));
				orderPanel.add(orderQuantityTextField);
					orderQuantityTextField.setBounds(460, 460, 100, 40);
					orderQuantityTextField.setForeground(Color.BLACK);
					orderQuantityTextField.setFont(orderQuantityTextField.getFont().deriveFont(20.0f));
				orderPanel.add(orderAddButton);
					orderAddButton.setBounds(40, 560, 130, 40);
					orderAddButton.setForeground(Color.BLACK);
					orderAddButton.setFont(orderAddButton.getFont().deriveFont(17.0f));
				orderPanel.add(orderRemoveButton);
					orderRemoveButton.setBounds(180, 560, 170, 40);
					orderRemoveButton.setForeground(Color.BLACK);
					orderRemoveButton.setFont(orderRemoveButton.getFont().deriveFont(15.0f));
				orderPanel.add(orderBackButton);
					orderBackButton.setBounds(360, 560, 150, 40);
					orderBackButton.setForeground(Color.BLACK);
					orderBackButton.setFont(orderBackButton.getFont().deriveFont(15.0f));
				orderPanel.add(orderPrint);
					orderPrint.setBounds(520, 560, 150, 40);
					orderPrint.setForeground(Color.BLACK);
					orderPrint.setFont(orderPrint.getFont().deriveFont(16.0f));
				JTabelValuePopulator pop = new JTabelValuePopulator();
				JTable createTable = pop.createTable(Order.class, OrderDao.findAll());
				orderPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
				orderBackButton.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						orderPanel.setVisible(false);
						mainPanel.setVisible(true);
						setContentPane(mainPanel);
						setSize(600, 600);
						setTitle("Order Manager");
					}

				});

				orderAddButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						OrderDao order = new OrderDao();
						ProductDao product = new ProductDao();
						int productId = Integer.parseInt(orderProductIdTextField.getText());
						int quantity = Integer.parseInt(orderQuantityTextField.getText());
						for (Product p : product.findAll()) {
							if (productId == p.getProductId()) {
								if (quantity > 0) {
									if (p.getProductStock() >= quantity) {
										order.insert(new Order(Integer.parseInt(orderIdTextField.getText()),
												Integer.parseInt(orderClientIdTextField.getText()), productId, quantity));
										product.update(p.getProductId(), new Product(p.getProductId(), p.getProductName(),
												p.getProductPrice(), (p.getProductStock() - quantity)));
										JTabelValuePopulator pop = new JTabelValuePopulator();
										JTable createTable = pop.createTable(Order.class, OrderDao.findAll());
										orderPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
									} else {
										JOptionPane.showMessageDialog(rootPane, "Out of Stock. We are Sorry!");
									}
								}else {
									JOptionPane.showMessageDialog(rootPane, "Enter a valid quantity!");
								}
							}
						}
						orderIdTextField.setText("");
						orderClientIdTextField.setText("");
						orderProductIdTextField.setText("");
						orderQuantityTextField.setText("");
					}
				});

				orderRemoveButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						OrderDao order = new OrderDao();
						order.remove(Integer.parseInt(orderIdTextField.getText()));
						orderIdTextField.setText("");
						orderClientIdTextField.setText("");
						orderProductIdTextField.setText("");
						orderQuantityTextField.setText("");
						JTabelValuePopulator pop = new JTabelValuePopulator();
						JTable createTable = pop.createTable(Order.class, OrderDao.findAll());
						orderPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
					}
				});

				orderPrint.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						OrderDao orderD = new OrderDao();
						Order order = new Order();
						order = orderD.findById(Integer.parseInt(orderIdTextField.getText()));
						ClientDAO clientD = new ClientDAO();
						Client client = new Client();
						client = clientD.findById(order.getOrderClientId());
						ProductDao productD = new ProductDao();
						Product product = new Product();
						product = productD.findById(order.getOrderProductId());
						System.out.println("Order number: " + order.getOrderId());
						System.out.println("Client Details: ");
						System.out.println(client);
						System.out.println("Product Details: ");
						System.out.println(product + " Quantity: " + order.getOrderQuantity());
						System.out.println(
								"Total Price to pay: " + (order.getOrderQuantity() * product.getProductPrice()));
						orderIdTextField.setText("");
						orderClientIdTextField.setText("");
						orderProductIdTextField.setText("");
						orderQuantityTextField.setText("");
					}
				});
			}
		});

		this.setContentPane(mainPanel);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	// public void customerInterface()
	// {
	// clientIdLabel.setVisible(true);
	// clientNameLabel.setVisible(true);
	// clientMailLabel.setVisible(true);
	// clientPhoneLabel.setVisible(true);
	// clientAgeLabel.setVisible(true);
	// clientIdTextField.setVisible(true);
	// clientNameTextField.setVisible(true);
	// clientMailTextField.setVisible(true);
	// clientPhoneTextField.setVisible(true);
	// clientAgeTextField.setVisible(true);
	// clientAddButton.setVisible(true);
	// clientRemoveButton.setVisible(true);
	// clientUpdateButton.setVisible(true);
	// clientBackButton.setVisible(true);
	// }
	// public void hideCustomerInterface()
	// {
	// clientIdLabel.setVisible(false);
	// clientNameLabel.setVisible(false);
	// clientMailLabel.setVisible(false);
	// clientPhoneLabel.setVisible(false);
	// clientAgeLabel.setVisible(false);
	// clientIdTextField.setVisible(false);
	// clientNameTextField.setVisible(false);
	// clientMailTextField.setVisible(false);
	// clientPhoneTextField.setVisible(false);
	// clientAgeTextField.setVisible(false);
	// clientAddButton.setVisible(false);
	// clientRemoveButton.setVisible(false);
	// clientUpdateButton.setVisible(false);
	// clientBackButton.setVisible(false);
	// }
	//
	// public void productInterface()
	// {
	// productIdLabel.setVisible(true);
	// productNameLabel.setVisible(true);
	// productPriceLabel.setVisible(true);
	// productStockLabel.setVisible(true);
	// productIdTextField.setVisible(true);
	// productNameTextField.setVisible(true);
	// productPriceTextField.setVisible(true);
	// productStockTextField.setVisible(true);
	// productAddButton.setVisible(true);
	// productRemoveButton.setVisible(true);
	// productUpdateButton.setVisible(true);
	// productBackButton.setVisible(true);
	// }
	// public void hideProductInterface()
	// {
	// productIdLabel.setVisible(false);
	// productNameLabel.setVisible(false);
	// productPriceLabel.setVisible(false);
	// productStockLabel.setVisible(false);
	// productIdTextField.setVisible(false);
	// productNameTextField.setVisible(false);
	// productPriceTextField.setVisible(false);
	// productStockTextField.setVisible(false);
	// productAddButton.setVisible(false);
	// productRemoveButton.setVisible(false);
	// productUpdateButton.setVisible(false);
	// productBackButton.setVisible(false);
	// }
	//
	// public void orderInterface()
	// {
	// orderIdLabel.setVisible(true);
	// orderClientIdLabel.setVisible(true);
	// orderProductIdLabel.setVisible(true);
	// orderQuantityLabel.setVisible(true);
	// orderIdTextField.setVisible(true);
	// orderClientIdTextField.setVisible(true);
	// orderProductIdTextField.setVisible(true);
	// orderQuantityTextField.setVisible(true);
	// orderAddButton.setVisible(true);
	// orderRemoveButton.setVisible(true);
	// orderBackButton.setVisible(true);
	// }
	// public void hideOrderInterface()
	// {
	// orderIdLabel.setVisible(false);
	// orderClientIdLabel.setVisible(false);
	// orderProductIdLabel.setVisible(false);
	// orderQuantityLabel.setVisible(false);
	// orderIdTextField.setVisible(false);
	// orderClientIdTextField.setVisible(false);
	// orderProductIdTextField.setVisible(false);
	// orderQuantityTextField.setVisible(false);
	// orderAddButton.setVisible(false);
	// orderRemoveButton.setVisible(false);
	// orderBackButton.setVisible(false);
	// }
	//
	// public void menuInterface()
	// {
	// clientButton.setVisible(true);
	// productButton.setVisible(true);
	// orderButton.setVisible(true);
	// }
	// public void hideMenuInterface()
	// {
	// clientButton.setVisible(false);
	// productButton.setVisible(false);
	// orderButton.setVisible(false);
	// }
/**
 * The main class starting the simulation
 * @param args
 * @throws ClassNotFoundException
 * @throws InstantiationException
 * @throws IllegalAccessException
 * @throws UnsupportedLookAndFeelException
 * @throws FileNotFoundException
 */
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException,
			IllegalAccessException, UnsupportedLookAndFeelException, FileNotFoundException {
		File file = new File("Receipt.txt");
		FileOutputStream fos = new FileOutputStream(file);
		PrintStream ps = new PrintStream(fos);
		System.setOut(ps);
		functionFrame aux = new functionFrame();
		aux.setVisible(true);
	}
}
